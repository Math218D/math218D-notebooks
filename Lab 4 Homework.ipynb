{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In today's class, you solved the exact same system of equations using LU as you did last week using row-reduction of an augmented matrix and back substitution. It certainly seems you had to do more work this time when you already had a way of solving equations. What is the advantage of using LU decomposition? This homework will explore this question.\n",
    "\n",
    "> ## Make a copy of this notebook (File menu -> Make a Copy...)\n",
    "\n",
    "Suppose that you are doing the same experiment on a number of different samples. You measure your outputs at the same time points, but get different results each time, depending on your sample. You want to fit polynomials to each of your data sets. As we saw when we fitted polynomials to data, this will involve solving an equation $Ax=b$ for different $b$'s, but always the same $A$.\n",
    "\n",
    "Consider the following table, showing the results of three such experiments:\n",
    "\n",
    "$t$ | 1 | 2 | 3 | 4 | 5 | 6 | 7\n",
    "--- | :---: | :---: | :---: |:---: |:---: |:---: |:---: |\n",
    "$y_1$ | 10 | 15 | -1 | 2 | -4 | 5 | 10\n",
    "$y_2$ | 10 | 13 | 0 | 2 | -3 | 5 | 11\n",
    "$y_3$ | 11 | 14 | -1 | 3 | -5 | 4 | 9\n",
    "\n",
    "We will fit polynomials to each of these and compare using row-reduction and back-substitution to LU decomposition. To do so, we'll need to do the following:\n",
    "* Understand the role of pivoting.<br><br>\n",
    "* Solve the equations using row-reduction and back-substitution.<br><br>\n",
    "* Lastly, compare this to solving them using LU decomposition, followed by forward- and back-substitution.\n",
    "\n",
    "## Pivoting\n",
    "\n",
    "Recall that LU decomposition with pivoting takes a matrix $A$ and returns matrices $P$, $L$, and $U$ so that $$PA=LU$$\n",
    "\n",
    "We are trying to solve $Ax=v$. If we have a matrix $P$, then we can multiply both sides by it to get $$PAx=Pv$$ But $PA=LU$, so this is equivalent to $$LUx=Pv.$$\n",
    "\n",
    "So all we need to do is multiply our $v$ by $P$ before we begin foward- or back-substitutions! Remember that each row in an augmented matrix represents one of the equations in the system. So all we are really doing here is swapping around the equations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Homework Question 1 \n",
    "\n",
    "Use your row-reduction code and your `backsub(U,v)` function to find the coefficients of a sixth degree polynomial that fits each of the above data sets. In each case, this will involve solving $Ax=b$. \n",
    "1. Explain why the matrix $A$ is the same in each case. What is it?<br><br>\n",
    "1. Write down the sixth degree polynomials in each case. Write the coefficients of each power to two decimal places."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Homework Question 2\n",
    "Write a function called `LUSolve(L,U,P,v)` that does the following given an LU decomposition of a matrix $A$:\n",
    "1. First, multiplies the vector $v$ by $P$, as we discussed was needed.<br><br>\n",
    "1. Solves $Ly=Pv$ by forward substition.<br><br>\n",
    "1. Lastly, solves $Ux=y$ to find the solution of $Ax=v$.\n",
    "\n",
    "Test your function on the data above. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that since the matrix $A$ is always the same, we only have to use our $LU$ decomposition code once! This is much faster than having to do the row-reduction over and over for each output vector. The LU decomposition encodes the process of row-reduction in the lower-triangular matrix $L$, thus avoiding the need to recompute it.\n",
    "\n",
    "Lastly, if you look at the data sets given above, you may notice that they are all quite similar to each other numerically. Yet the polynomials you generated are rather vastly different from each other. This is a serious problem. We say that the polynomial model has high *variance*. We will study this further in future labs.\n",
    "\n",
    "### Homework Question 3\n",
    "\n",
    "Write code that takes a set of *n* times (as a vector) and the outcomes of a number (say, *m*) of different experiments with measurements at those times (as an $m\\times n$ array), and returns the coefficients of polynomials that fit each set of measurements. Your code should use LU decomposition and your `LUSolve(L,U,P,v)` function to make it as efficient as possible. Test your code on the above data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Homework Question 4 \n",
    "\n",
    "Suppose you have a number of different output vectors $\\vec{c}$ for the same set of equations. We have two different ways of solving $A\\vec{x}=\\vec{c}$:\n",
    "\n",
    "* Row reduce the augmented matrix $[A|\\vec{c}]$, then back substitute. Repeat for every different $\\vec{c}$.<br><br>\n",
    "\n",
    "* Find the $PA=LU$ decomposition of $A$, then use our `LUSolve(L,U,P,v)` function we wrote above.\n",
    "\n",
    "Explain why we expect the second method to be far more efficient than the first if we have many different output vectors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Homework Question 5\n",
    "\n",
    "Let's examine once again the *LU* decomposition of the matrix from the last homework: $$A=\\begin{bmatrix} 10^{-4} & 0 & 10^4 \\\\ 10^4 & 10^{-4} & 0 \\\\ 0 & 10^4 & 1\\end{bmatrix}.$$\n",
    "\n",
    "As you saw in the lab, the code for *LU* decomposition without pivoting results in matrices *L* and *U* such that $A\\neq LU$.\n",
    "\n",
    "* By looking back at Question 4 from the lab and the work you did on floating point errors on Homework 3, explain exactly why you get the incorrect result you saw.\n",
    "\n",
    "* Compute by hand the *PA=LU* decomposition for this matrix. Do you still expect a floating point error to occur? Explain why in this case, we still get the right answer using our `LU(A)` code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Optional Bonus Question\n",
    "\n",
    "Write code that takes the coefficients of a polynomial and prints the polynomial with the coefficients printed to two decimal places. You should research Python functions that help you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
