{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Determinants\n",
    "\n",
    "In class, we encountered the notion of the *determinant* of a square matrix. Today, we will study how determinants are computed in practice. Along the way, we will experimentally do some *algorithm analysis*. That is, we will find that certain ways of computing the very same quantity can be much more efficient than others.\n",
    "\n",
    "> ## Make a copy of this notebook (File menu -> Make a Copy...)\n",
    "\n",
    "First, a reminder of work from class:\n",
    "\n",
    "**Question 1** \n",
    "1. If $I_n$ is the $n\\times n$ identity matrix, then what is $det(I_n)=|I_n|$?<br><br>\n",
    "1. If $A$ and $B$ are two $n\\times n$ matrices, then what is another way to express $|AB|$?<br><br>\n",
    "1. Suppose that $A$ is an $n\\times n$ matrix, and let its $(i,j)$ entry be denoted $a_{i,j}$. What is the definition of the *minor* $M_{i,j}$?<br><br>\n",
    "1. Suppose that $i=1$. Write a formula for $|A|$ in terms of $a_{1,j}$ and $M_{1,j}$ for $1\\leq j\\leq n$ (or take $i=0$ and $0\\leq j < n$ if you prefer NumPy notation).<br><br>\n",
    "1. Suppose that $U$ is an upper triangular matrix. Use the formula you wrote down above to find a simple expression for $|U|$.<br><br>\n",
    "1. What can you say about the determinant of $A^T$, the transpose of $A$?<br><br>\n",
    "1. Use your answer to the second question above, and the question you just answered, to show that the determinant of an orthonormal matrix $Q$ must be $1$ or $-1$.\n",
    "\n",
    "\n",
    "## Computing Determinants\n",
    "\n",
    "**Question 2** First, write a function called `det2(A)` that takes a $2\\times 2$ matrix and returns its determinant. While you're at it, build in some error-checking: make sure that your function only accepts $2\\times 2$ matrices. In Python, errors are *raised*:\n",
    "```python\n",
    "raise ValueError('The matrix is not a 2x2!')\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3** Write a function `minor(A,i,j)` that returns the $(i,j)$ minor of a matrix $A$. Two ways to do this are: create a matrix of the right size, then fill it with four different slices of your original matrix (this is the fastest, I believe); or research and use the `np.delete()` function. There are also other ways!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 4** Use the two functions you just wrote and the answer to the question in the fourth bullet point of Question 1 to write a function called `det3(A)` that computes the determinant of a $3\\times 3$ matrix. Test out your code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 5** A *recursive* function is a function that calls itself. For example, consider the function:\n",
    "```python\n",
    "def prod(lst):\n",
    "    ans = 1    \n",
    "    if len(lst) == 1:\n",
    "        ans = lst[0]\n",
    "    else:\n",
    "        ans = lst[0] * prod(lst[1:])    \n",
    "    return ans\n",
    "```\n",
    "\n",
    "Explain what this function does given a list of numbers and how it does it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 6** Write a recursive function `det(A)` that takes a square matrix (make sure you check it's square!) and returns its determinant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analyzing your Code\n",
    "\n",
    "**Question 7** By using the `np.random.randint(n,n)` command, generate a series of square matrices from size $3\\times 3$ to $9\\times 9$. Then run \n",
    "```python\n",
    "%timeit det(A)\n",
    "```\n",
    "on each of them. Comment on your results. Approximately how long do you think it would take your code to compute the determinant of a $10\\times 10$ matrix? Test that out!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 8** By considering how many multiplications your code executes to compute the determinant of an $n\\times n$ matrix, explain your timing results above.\n",
    "\n",
    "As you can probably see now, it would be really impractical to use this code to compute the determinant of, say, a $100\\times 100$ matrix. We need to find a better way!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using Decompositions to Compute Determinants\n",
    "\n",
    "**Question 9** Suppose you had an QR decomposition of a matrix $A$. That is, you know that $A=QR$, where $Q$ is orthonormal and $R$ is upper triangular. \n",
    "1. What do you know about the determinant of $Q$?<br><br>\n",
    "1. Suppose you knew the determinant of $R$. Would you be able to determine the determinant of $A$? If not, how close could you get?<br><br>\n",
    "1. Why is it very easy (and quick!) to compute the deteminant of $R$?<br><br>\n",
    "1. Write a function called `QRdet(A)` that uses your QR code from the last lab to compute the determinant of $A$ as best you can (given the limitation you wrote down above). Test your code by generating some matrices and comparing to the determinants your previous function computed.<br><br>\n",
    "1. Run timing tests for your new function on random matrices from size 3 to size 9. How do your answers compare to the ones above? Is it feasible to use your code to compute the determinant of a $100\\times 100$ matrix? If you think so, try! "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 10** Suppose we have an $LU$ decomposition of a matrix $A$: $PA=LU$. \n",
    "1. How can you easily (and quickly) compute the determinant of $U$?<br><br>\n",
    "1. What is the determinant of $L$? Why?<br><br>\n",
    "1. What else do you need to know to completely determine the determinant of $A$?\n",
    "\n",
    "### Determinants of Permutation Matrices\n",
    "\n",
    "The matrix $P$ in the decomposition $PA=LU$ is a permutation matrix. Recall that all it does is swap the rows of $A$. \n",
    "\n",
    "**Question 11** \n",
    "1. How do you transform the identity matrix into a permutation matrix?<br><br>\n",
    "1. Starting from the identity matrix, swap two of its rows. What is the determinant of the resulting matrix?<br><br>\n",
    "1. Swap rows again. What is the determinant now?<br><br>\n",
    "1. Once more! What is the determinant now?<br><br>\n",
    "1. Suppose that to get $P$, we swapped rows $k$ times. What is the determinant of $P$?\n",
    "\n",
    "**Question 12** Based on your LU code, write a function called `LUdet(A)` that uses the LU decompsition to compute the determinant of $A$. Test your code and run some timing tests. How does this compare to your QR-based determinant code above?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
